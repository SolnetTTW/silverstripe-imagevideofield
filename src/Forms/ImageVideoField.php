<?php

namespace Solnet\Forms;

use SilverStripe\Forms\CompositeField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\FieldGroup;
use SilverStripe\Forms\FormField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\View\Requirements;
use SilverShop\HasOneField\HasOneButtonField;
use gorriecoe\Embed\Models\Video;

class ImageVideoField extends CompositeField
{
    /**
     * Usage:
     * private static $db = [
     *     'NameOfBooleanField' => 'Boolean',
     * ];
     *
     * private static $has_one = [
     *     'NameOfImageField' => SilverStripe\Assets\Image::class,
     *     'NameOfVideoField' => gorriecoe\Embed\Models\Video::class,
     * ];
     *
     * public function getCMSFields()
     * {
     *     $field = ImageVideoField::create(
     *         'NameOfBooleanField', // false=use image (default), true=use video
     *         'Title of field',     // Displayed in UI
     *         'NameofImageField',   // Field uploaded image is saved to (if any)
     *         'NameOfVideoField'    // Field embedded video is saved to (if any)
     *     );
     * }
     *
     * @param string $name
     * @param string|FormField $checkboxField Name of checkbox field, or a field to use for the checkbox field
     * @param string|FormField $imageField Name of Image field, or a field to use for the image field
     * @param string|FormField $videoField Name of Video field, or a field to use for the video field
     * @param DataObject $parent The dataobject this field will be operating on
     */
    public function __construct($name, $checkboxField, $imageField, $videoField, $parent)
    {
        // Create checkbox field
        if (!($checkboxField instanceof FormField)) {
            $checkboxField = CheckboxField::create(
                $checkboxField,
                _t('ImageVideoField.Checkbox_Title', 'Use video instead of image?')
            );
        }
        // Create Image field
        if (!($imageField instanceof FormField)) {
            $imageField = UploadField::create(
                $imageField,
                _t('ImageVideoField.Image_Title', 'Image')
            );
        }
        // Create videofield
        if (!($videoField instanceof FormField)) {
            $videoField = FieldGroup::create(
                [
                    LiteralField::create(
                        $videoField.'_Demo',
                        $parent->Video() ? $parent->Video()->renderWith('Solnet/ORM/ImageVideoField/Preview') : ''
                    ),
                    HasOneButtonField::create(
                        $videoField,
                        _t('ImageVideoField.Video_Title', 'Video'),
                        $parent
                    ),
                ]
            );
        }

        $checkboxField->addExtraClass('solnet-imagevideofield-checkbox');
        $imageField->addExtraClass('solnet-imagevideofield-image');
        $videoField->addExtraClass('solnet-imagevideofield-video');

        parent::__construct([$checkboxField, $imageField, $videoField]);
        $this->setName($name);
        $this->setTitle($imageField->Title());
        $this->addExtraClass('solnet-imagevideofield');
    }

    /**
     * Output the field into the CMS template.
     *
     * Adds additional JavaScript to hide/show fields according to the ShowVideo checkbox.
     */
    public function Field($properties = [])
    {
        Requirements::javascript('solnet/silverstripe-imagevideofield:client/dist/js/ImageVideoFieldCMS.js');
        return parent::Field($properties);
    }
}
