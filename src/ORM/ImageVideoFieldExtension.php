<?php

namespace Solnet\ORM;

use gorriecoe\Embed\Models\Video;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use Solnet\Forms\ImageVideoField;

/**
 * Extension allows updating a single, configurable field on a DataObject from 
 * a has_one to SilverStripe\Assets\Image to an ImageVideoField.
 *
 * Useful for quickly enabling CMS switching between image/video on an existing item
 * that currently only outputs an image.
 *
 * If your object has more than one has_one to Image, you'll need to either specify which
 * field to attach this feature to via YAML:
 *
 * Page:
 *   extensions:
 *     - Solnet\ORM\ImageVideoFieldExtension
 *   imagevideofield_imagefield: 'MyImage'
 */

class ImageVideoFieldExtension extends DataExtension
{
    private static $db = [
        'ShowVideo' => 'Boolean',
    ];

    private static $has_one = [
        'Video' => Video::class,
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeByName(['ShowVideo', 'VideoID']);

        $imageField = clone $this->getImageField($fields);
        if (!$imageField) {
            user_error(
                _t('ImageVideoField.NoImageFieldError', 'No Image field was found on class.')
            );
        }

        // Insert a hidden field that we can later remove
        $placeholderFieldName = $imageField->getName() . '_placeholder';
        $fields->insertAfter(
            $imageField->getName(),
            HiddenField::create($placeholderFieldName)
        );

        // Remove the original Image field
        $fields->removeByName($imageField->getName());

        $fields->insertAfter(
            $placeholderFieldName,
            ImageVideoField::create(
                $imageField->getName().'_ImageVideoField',
                'ShowVideo',
                $imageField->getName(),
                'Video',
                $this->owner
            )
        );

        // Remove the placeholder
        $fields->removeByName($placeholderFieldName);
    }

    protected function getImageField($fields)
    {
        // Find correct field to use as Image field
        $fieldName = $this->owner->config()->get('imagevideofield_imagefield');
        // Is there a field with a matching name from configuration?
        if ($fieldName) {
            $imageField = $fields->dataFieldByName($fieldName);
            if ($imageField) {
                return $imageField;
            }
        }
        // Nothing explicitly configured - find first link to Image class
        foreach ($this->owner->hasOne() as $fieldName => $class) {
            if ($class === Image::class) {
                return $fields->dataFieldByName($fieldName);
            }
        }
        // Nothing found.
        return null;
    }
}
