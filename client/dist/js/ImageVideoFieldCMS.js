(function($) {
    $.entwine('ss', function($){
        $('div.field.solnet-imagevideofield').entwine({
            onadd:function(){
                this.toggleFields();
            },
            onchange:function(){
                this.toggleFields();
            },
            toggleFields:function(){
                var checkboxField = this.find('.solnet-imagevideofield-checkbox').find('input[type=checkbox][name=ShowVideo]'),
                    imageField = this.find('.solnet-imagevideofield-image'),
                    videoField = this.find('.solnet-imagevideofield-video');
  
                if (checkboxField.prop('checked')) {
                    imageField.hide();
                    videoField.show();
                } else {
                    imageField.show();
                    videoField.hide();
                }
            }
        });
    });
}(jQuery));
